﻿using System.Collections.ObjectModel;

namespace IFCE.TodoList.Domain.Model;

public class User : Entity
{
    public string Name { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    
    // EF Relation
    public virtual Collection<Assignment> Assignments { get; set; } = new();
    public virtual Collection<AssignmentList> AssignmentLists { get; set; } = new();
}