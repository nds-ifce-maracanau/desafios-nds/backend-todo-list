﻿namespace IFCE.TodoList.Domain.Contracts;

public interface IUnitOfWork
{
    Task<bool> Commit();
}