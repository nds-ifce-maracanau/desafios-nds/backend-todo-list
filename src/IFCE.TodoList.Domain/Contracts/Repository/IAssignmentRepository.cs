﻿using IFCE.TodoList.Domain.Filter;
using IFCE.TodoList.Domain.Model;

namespace IFCE.TodoList.Domain.Contracts.Repository;

public interface IAssignmentRepository : IRepository<Assignment>
{
    Task<IPagedResult<Assignment>> Search(Guid userId, AssignmentFilter filter, int perPage = 10, 
        int page = 1, Guid? listId = null);
    Task<Assignment> GetById(Guid id, Guid userId);
    void Add(Assignment assignment);
    void Edit(Assignment assignment);
    void Delete(Assignment assignment);
}