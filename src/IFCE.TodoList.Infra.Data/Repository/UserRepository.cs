﻿using IFCE.TodoList.Domain.Model;
using Microsoft.EntityFrameworkCore;
using IFCE.TodoList.Domain.Contracts;
using IFCE.TodoList.Infra.Data.Context;
using IFCE.TodoList.Domain.Contracts.Repository;

namespace IFCE.TodoList.Infra.Data.Repository;

public class UserRepository : IUserRepository
{
    private readonly ApplicationDbContext _context;

    public UserRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public IUnitOfWork UnitOfWork => _context;

    public async Task<User> FindByEmail(string email)
    {
        return await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
    }

    public async Task<bool> IsEmailInUse(string email)
    {
        return await _context.Users.AnyAsync(u => u.Email == email);
    }
    
    public void Add(User user)
    {
        _context.Users.Add(user);
    }

    public void Dispose()
    {
        _context?.Dispose();
    }
}