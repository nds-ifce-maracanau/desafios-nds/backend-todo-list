﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace IFCE.TodoList.Infra.Data.Migrations
{
    public partial class UpdateConcludedField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CloncludedAt",
                table: "Assignments",
                newName: "ConcludedAt");

            migrationBuilder.RenameColumn(
                name: "Cloncluded",
                table: "Assignments",
                newName: "Concluded");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ConcludedAt",
                table: "Assignments",
                newName: "CloncludedAt");

            migrationBuilder.RenameColumn(
                name: "Concluded",
                table: "Assignments",
                newName: "Cloncluded");
        }
    }
}
