﻿namespace IFCE.TodoList.Application.DTO.AssignmentList;

public class EditAssignmentListDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }
}