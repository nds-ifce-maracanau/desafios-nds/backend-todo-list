﻿namespace IFCE.TodoList.Application.DTO.AssignmentList;

public class AddAssignmentListDto
{
    public string Name { get; set; }
}