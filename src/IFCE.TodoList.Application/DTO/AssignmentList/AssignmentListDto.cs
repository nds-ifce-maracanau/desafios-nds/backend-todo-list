﻿using IFCE.TodoList.Application.DTO.Assignment;

namespace IFCE.TodoList.Application.DTO.AssignmentList;

public class AssignmentListDto
{
    public Guid Id { get; set; }
    public string Name { get; set; }

    public List<AssignmentDto> Assignments { get; set; }
}