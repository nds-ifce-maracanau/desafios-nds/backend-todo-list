﻿using FluentValidation;
using IFCE.TodoList.Domain.Model;

namespace IFCE.TodoList.Application.Validations;

public class AssignmentListValidator : AbstractValidator<AssignmentList>
{
    public AssignmentListValidator()
    {
        RuleFor(c => c.Name)
            .NotEmpty()
            .NotNull();

        RuleFor(c => c.UserId)
            .NotNull()
            .NotEqual(Guid.Empty);
    }
}