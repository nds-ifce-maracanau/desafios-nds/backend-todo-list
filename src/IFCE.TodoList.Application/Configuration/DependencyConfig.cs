﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using IFCE.TodoList.Domain.Model;
using Microsoft.AspNetCore.Identity;
using ScottBrady91.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using IFCE.TodoList.Application.Services;
using IFCE.TodoList.Application.Notifications;
using IFCE.TodoList.Application.Contracts.Services;
using IFCE.TodoList.Application.Validations;

namespace IFCE.TodoList.Application.Configuration;

public static class DependencyConfig
{
    public static void ResolveDependencies(this IServiceCollection services)
    {
        services
            .AddScoped<INotificator, Notificator>()
            .AddScoped<IPasswordHasher<User>, Argon2PasswordHasher<User>>();

        services
            .AddScoped<IAuthService, AuthService>()
            .AddScoped<IAssignmentService, AssignmentService>()
            .AddScoped<IAssignmentListService, AssignmentListService>();
        
        services
            .AddScoped<IValidator<Assignment>, AssignmentValidator>()
            .AddScoped<IValidator<AssignmentList>, AssignmentListValidator>();
        
        services
            .AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
    }
}