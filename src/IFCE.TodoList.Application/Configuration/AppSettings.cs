﻿namespace IFCE.TodoList.Application.Configuration;

public class AppSettings
{
    public string Secret { get; set; }
    public string Issuer { get; set; }
    public string Audience { get; set; }
    public int ExpiracaoToken { get; set; }
}