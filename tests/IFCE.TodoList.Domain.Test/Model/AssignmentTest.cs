﻿using Xunit;
using FluentAssertions;
using FluentAssertions.Execution;
using IFCE.TodoList.Domain.Model;

namespace IFCE.TodoList.Domain.Test.Model;

public class AssignmentTest
{
    [Fact]
    public void SetConcluded_MustSetConcludedAndConcludedAt()
    {
        // Arrange
        var assignment = new Assignment();

        // Act
        assignment.SetConcluded();
        
        // Assert
        using (new AssertionScope())
        {
            assignment.Concluded.Should().BeTrue();
            assignment.ConcludedAt.Should().NotBeNull();
        }
    }
    
    [Fact]
    public void SetUnconcluded_MustUnsetConcludedAndConcludedAt()
    {
        // Arrange
        var assignment = new Assignment();
        assignment.SetConcluded();

        // Act
        assignment.SetUnconcluded();
        
        // Assert
        using (new AssertionScope())
        {
            assignment.Concluded.Should().BeFalse();
            assignment.ConcludedAt.Should().BeNull();
        }
    }
}